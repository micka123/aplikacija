<?php
session_start();
include_once 'Dbconnect.php';
$username = $_SESSION['user'];
//echo "".$username."";
$q =  mysqli_query($con,"SELECT `Br_Indeks` FROM studenti WHERE `username`='$username' ");
$red = mysqli_fetch_array($q);
//echo "".$red['Br_Indeks']."";



        ?>


<DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Anketa</title>
        
        <link href="myStyle2.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <form name="Anketa" action="anketa_profesor_predmet.php" method="post">
            <div id="content2">
		<div id="naslovi">
			<h3>Универзитет „Св. Климент Охридски“- Битола</h3>
			<h2>Факултет за информатички и комуникациски технологии – Битола</h2>
			<h3>Комисија за самоевалуација</h3>
		</div><br><br>
		<div class="inf">
                    <input type="hidden" name="korisnik" value="<?php echo "".$red['Br_Indeks']."";?>">
			<label for="datum">Датум на спроведување на анкета:</label>
			<input type="text" value="" name="datum" id="datum" class="informacii" placeholder="Ден-Месец-Година">
                        <link href="jquery-ui-1.11.4.custom/jquery-ui-1.11.4.custom/jquery-ui.css">
                        <script src="jquery-ui-1.11.4.custom/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
                        <script src="jquery-ui-1.11.4.custom/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
                        <script >
                       //     var date = $('#datum').datepicker({dateFormat:'dd/mm/yy'}).val();
                            var today = new Date();
                            var dd = today.getDate();
                            var mm = today.getMonth()+1;
                            var yyyy = today.getFullYear();
                           if (dd<10)
                           {
                              dd = '0' + dd;
                                            
                          }
                          if(mm<10)
                           {
                               mm= '0'+mm;
                          }
                           var today = dd +'/'+mm+'/'+yyyy;
                           document.getElementById("datum").value = today;
                            </script>
		</div><br>
		<div class="inf">
			<label for="sPrograma">Студиска програма:</label>
                        <?php
                        include_once 'Dbconnect.php';
                        mysqli_query($con,"SET NAMES utf8;");
                        mysqli_set_charset($con, 'utf-8');
                        $query = mysqli_query($con,"SELECT `id_studiska_programa`,`ime_studiska_programa` FROM studiska_programa ");
                      echo  '<select id="sPrograma" name="studiska_programa" class="informacii" placeholder="Внесете студиска програма">';
                        while ($row = mysqli_fetch_array($query)){
			echo '<option selected disabled hidden > </option>';
                        echo '<option value="'.$row['id_studiska_programa'].'">'.$row['ime_studiska_programa'].'</option>';
                                
                        }
                          
                            echo '</select>';
                                ?>
		</div><br>
		<div class="inf">
			<label for="nazivPredmet">Назив на предметот:</label>
                        <?php
                         include_once 'Dbconnect.php';  
                              mysqli_query($con,"SET NAMES utf8;");
                        mysqli_set_charset($con, 'utf-8');
                        $query = mysqli_query($con,"SELECT `id_predmet`,`ime_predmet` FROM predmeti ");
			echo '<select name="ime_predmet" id="nazivPredmet" class="informacii" placeholder="Внесете го името на предметот">';
                        while ($row = mysqli_fetch_array($query)){
			echo '<option selected disabled hidden></option>';
                         echo   '<option value="'.$row['id_predmet'].'">'.$row['ime_predmet'].'</option>';
                        }                          
           echo '</select>';
                                ?>
		</div><br>
		<div class="inf">
			<label for="profesor">Име и презиме на предметниот наставник:</label>
                        <?php
                        include_once 'Dbconnect.php';  
                              mysqli_query($con,"SET NAMES utf8;");
                        mysqli_set_charset($con, 'utf-8');
                        $query = mysqli_query($con,"SELECT `id_profesor`,`ime` FROM profesori ");
			echo '<select name="ime_profesor" id="profesor" class="informacii" placeholder="Име и презиме на професор">';
                        while ($row = mysqli_fetch_array($query)){
                            
                        echo'<option selected disabled hidden></option>';
                        echo '<option value="'.$row['id_profesor'].'">'.$row['ime'].'</option>';
                        }
echo '</select>';
                                ?>
		</div><br><br>
		
		<div class="inf">
			<label id="studAnketa">СТУДЕНТСКА АНКЕТА:</label><br>
			<p id="naslovAnketa">ОЦЕНУВАЊЕ НА РЕАЛИЗАЦИЈАТА НА НАСТАВНО-ОБРАЗОВНАТА ДЕЈНОСТ НА АКАДЕМСКИОТ КАДАР</p>
		</div><br>
<table id="tabela4" cellspacing="0" border="1px">
<thead> 
<tr>

<th colspan="8">I. Настава</th>
<th colspan="5">1</th>
<th colspan="5">2</th>
<th colspan="5">3</th>
<th colspan="5">4</th>
<th colspan="5">5</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="8" class="prasanja">1. Наставникот е соодветно подготвен за квалитетна реализација на наставата</td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa1" value="1"  id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa1" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa1" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa1" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa1" value="5" id="radio5"></td>
</tr>

<tr>
<td colspan="8" class="prasanja">2. Наставникот за време на предавањата е посветен  и предизвикува интерес кај студентите</td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa2" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa2" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa2" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa2" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa2" value="5" id="radio5"></td>
</tr>

<tr>
<td colspan="8" class="prasanja">3. Наставникот користи интерактивни методи на настава и ги мотивира студентите за вклучување во наставниот процес</td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa3" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa3" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa3" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa3" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa3" value="5" id="radio5"></td>
</tr>

<tr>
<td colspan="8" class="prasanja">4. Наставникот стимулира дополнителна активност на студентите</td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa4" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa4" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa4" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa4" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa4" value="5" id="radio5"></td>
</tr>

<tr>
<td colspan="8" class="prasanja">5. Дополнителните активности се во функција на зголемување и продлабочување на знаењата од предметот</td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa5" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa5" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa5" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa5" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa5" value="5" id="radio5"></td>
</tr>

<tr>
<td colspan="8" class="prasanja">6. Предметната програма ги прошири и продлабочи вашите знаења</td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa6" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa6" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa6" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa6" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa6" value="5" id="radio5"></td>
</tr>

<tr>
<td colspan="8" class="prasanja">7. Фондот на часови за предавање и вежби е соодветен на обемот и тежината на предметната програма</td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa7" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa7" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa7" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa7" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa7" value="5" id="radio5"></td>
</tr>
</tbody>
</table>
<br><br>


<table id="tabela5" cellspacing="0" border="1px">
<thead> 
<tr>

<th colspan="8" class="prasanja">II. Редовност</th>
<th colspan="5">1</th>
<th colspan="5">2</th>
<th colspan="5">3</th>
<th colspan="5">4</th>
<th colspan="5">5</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="8" class="prasanja">8. Наставникот редовно ја изведува наставата</td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa8" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa8" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa8" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa8" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa8" value="5" id="radio5"></td>
</tr>

<tr>
<td colspan="8" class="prasanja">9. Наставникот е отворен  и достапен за консултации  и соработка</td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa9" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa9" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa9" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa9" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa9" value="5" id="radio5"></td>
</tr>
</tbody>
</table>
<br><br>


<table id="tabela6" cellspacing="0" border="1px">
<thead> 
<tr>

<th colspan="8" class="prasanja">III. Однос кон студентите</th>
<th colspan="5">1</th>
<th colspan="5">2</th>
<th colspan="5">3</th>
<th colspan="5">4</th>
<th colspan="5">5</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="8" class="prasanja">10.	Личната култура  и односот на наставникот се на соодветно ниво</td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa10" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa10" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa10" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa10" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa10" value="5" id="radio5"></td>
</tr>
</tbody>
</table>
<br><br>


<table id="tabela7" cellspacing="0" border="1px">
<thead> 
<tr>

<th colspan="8" class="prasanja">IV. Оценување</th>
<th colspan="5">1</th>
<th colspan="5">2</th>
<th colspan="5">3</th>
<th colspan="5">4</th>
<th colspan="5">5</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="8" class="prasanja">11.	Наставникот објективно ги оценува  резултатите од работата на студентите</td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa11" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa11" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa11" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa11" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa11" value="5" id="radio5"></td>
</tr>

<tr>
<td colspan="8" class="prasanja">12. Содржината и структурата на испитните прашања овозможуваат  објективно да се оцени нивото на  совладаност на материјалот</td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa12" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa12" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa12" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa12" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa12" value="5" id="radio5"></td>
</tr>

<tr>
<td colspan="8" class="prasanja">13. Предметната програма е современа, овозможува стекнување со квалитетни и применливи знаења и вештини</td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa13" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa13" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa13" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa13" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa13" value="5" id="radio5"></td>
</tr>
</tbody>
</table>
<br><br>




<table id="tabela8" cellspacing="0" border="1px">
<thead> 
<tr>

<th colspan="8" class="prasanja">V. Литература</th>
<th colspan="5">1</th>
<th colspan="5">2</th>
<th colspan="5">3</th>
<th colspan="5">4</th>
<th colspan="5">5</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="8" class="prasanja">14. За предметот постои соодветна основна и дополнителна литература.</td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa14" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa14" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa14" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa14" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa14" value="5" id="radio5"></td>
</tr>

<tr>
<td colspan="8" class="prasanja">15. Испитните прашања се во рамките на предметната програма и предвидената основна литература </td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa15" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa15" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa15" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa15" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa15" value="5" id="radio5"></td>
</tr>

<tr>
<td colspan="8" class="prasanja">16. Обемот, содржината и тежината на предметната програма кореспондира со профилот на студиската програма</td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa16" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa16" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa16" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa16" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa16" value="5" id="radio5"></td>
</tr>
</tbody>
</table>
	<div id="divOcenuvanje">
		<label>Се оценува на скала од 1 – 5  (1 најниска оценка)</label><br><br>
		
                <input id="vnesiKopce" type="submit" name="submit" value="Внеси">
	</div>
</div>
        </form>

       
    </body>
</html>
