
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Anketa studiska programa</title>
        <link rel="stylesheet" type="text/css" href="myStyle2.css">
        <link rel="stylesheet" href="css/jquery.autocomplete.css"/>
    </head>
    <body>
        <form name="Anketa" action="anketa_studiska_programa.php"  >
            <div id="content2">
	<div id="naslovi">
		<h3>Универзитет „Св. Климент Охридски“- Битола</h3>
		<h2>Факултет за информатички и комуникациски технологии – Битола</h2>
		<h3>Комисија за самоевалуација</h3>
	</div><br><br>
	<div class="inf">
		<label for="datum">Датум на спроведување на анкета:</label>
		<input type="date" name="datum" id="datum" class="informacii2" placeholder="Ден-Месец-Година">
	</div><br>
	<div class="inf">
		<label for="sPrograma">Студиска програма:</label>
                <?php
                        include_once 'Dbconnect.php';
                        mysqli_query($con,"SET NAMES utf8;");
                        mysqli_set_charset($con, 'utf-8');
                        $query = mysqli_query($con,"SELECT `id_studiska_programa`,`ime_studiska_programa` FROM studiska_programa ");
		echo '<select id="sPrograma" class="informacii2" placeholder="Внесете студиска програма">';
                while ($row = mysqli_fetch_array($query)){
			echo'<option selected disabled hidden ></option>';
			echo'<option value="'.$row['id_studiska_programa'].'">'.$row['ime_studiska_programa'].'</option>';
                   
                }
               
			echo'</select>';
                        ?>
	</div><br>
	<div class="inf">
		<label for="vidNaStudii">Вид на студии:</label>
                        <?php
                        include_once 'Dbconnect.php';
                        mysqli_query($con,"SET NAMES utf8;");
                        mysqli_set_charset($con, 'utf-8');
                        $query = mysqli_query($con,"SELECT `id_vid_na_studii`,`vid_na_studii` FROM vid_na_studii ");
               echo' <select id="vidNaStudii" name="vid_na_studii" class="informacii2" placeholder="Академски/стучни, 3 годишни/4 годишни">';
                 while ($row = mysqli_fetch_array($query)){
                 
			echo'<option selected disabled hidden></option>';
			
                           echo' <option value="'.$row['id_vid_na_studii'].'">'.$row['vid_na_studii'].'</option>';
                 }
			echo'</select>';
                        ?>
	</div><br><br>
	<div class="inf">
		<p id="naslovAnketa">ОЦЕНУВАЊЕ НА СТУДИСКАТА ПРОГРАМА (НАСОКАТА) И УСЛОВИТЕ ЗА СТУДИРАЊЕ</p>
	</div><br>
<table id="tabela1" cellspacing="0" border="1px">
<thead> 
<tr>

<th colspan="8" class="prasanja">Оценување на квалитетот на студиската програма</th>
<th colspan="5">1</th>
<th colspan="5">2</th>
<th colspan="5">3</th>
<th colspan="5">4</th>
<th colspan="5">5</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="8" class="prasanja">1. Програмата е современа и атрактивна</td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa1" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa1" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa1" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa1" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa1" value="5" id="radio5"></td>
</tr>

<tr>
<td colspan="8" class="prasanja">2. Програмата овозможува стекнување на соодветни знаења и вештини</td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa2" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa2" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa2" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa2" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa2" value="5" id="radio5"></td>
</tr>

<tr>
<td colspan="8" class="prasanja">3. Структурата на студиската програма (задолжителни и изборни предмети) е во согласност со целниот профил</td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa3" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa3" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa3" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa3" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa3" value="5" id="radio5"></td>
</tr>

<tr>
<td colspan="8" class="prasanja">4. Програмата овозможува стекнување применливи (апликативни) знаења</td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa4" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa4" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa4" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa4" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa4" value="5" id="radio5"></td>
</tr>

<tr>
<td colspan="8" class="prasanja">5. Постои логичен редослед и поврзаност на предметните програми </td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa5" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa5" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa5" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa5" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa5" value="5" id="radio5"></td>
</tr>

<tr>
<td colspan="8" class="prasanja">6. Непостои непотребно преклопување на материјалот кај некои предмети</td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa6" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa6" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa6" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa6" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa6" value="5" id="radio5"></td>
</tr>
</tbody>
</table>
<br><br>



<table id="tabela2" cellspacing="0" border="1px">
<thead> 
<tr>

<th colspan="8" class="prasanja">Оценување на организацијата и реализацијата на студиската програма</th>
<th colspan="5">1</th>
<th colspan="5">2</th>
<th colspan="5">3</th>
<th colspan="5">4</th>
<th colspan="5">5</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="8" class="prasanja">1. Организација на наставата </td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa7" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa7" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa7" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa7" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa7" value="5" id="radio5"></td>
</tr>

<tr>
<td colspan="8" class="prasanja">2. Организација на колоквиуми и испити </td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa8" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa8" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa8" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa8" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa8" value="5" id="radio5"></td>
</tr>
<tr>
<td colspan="8" class="prasanja">3. Застапеност на практичната настава</td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa9" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa9" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa9" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa9" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa9" value="5" id="radio5"></td>
</tr>
<tr>
<td colspan="8" class="prasanja">4. Квалитет на практичната настава</td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa10" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa10" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa10" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa10" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa10" value="5" id="radio5"></td>
</tr>
<tr>
<td colspan="8" class="prasanja">5. Студиската програма ги исполнува вашите очекувања</td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa11" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa11" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa11" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa11" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa11" value="5" id="radio5"></td>
</tr>
</tbody>
</table>
<br><br>


<table id="tabela3" cellspacing="0" border="1px">
<thead> 
<tr>

<th colspan="8" class="prasanja">Услови за студирање на факултетот/високата школа</th>
<th colspan="5">1</th>
<th colspan="5">2</th>
<th colspan="5">3</th>
<th colspan="5">4</th>
<th colspan="5">5</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="8" class="prasanja">1. Информираност на студентите</td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa12" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa12" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa12" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa12" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa12" value="5" id="radio5"></td>
</tr>

<tr>
<td colspan="8" class="prasanja">2. Административна поддршка</td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa13" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa13" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa13" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa13" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa13" value="5" id="radio5"></td>
</tr>

<tr>
<td colspan="8" class="prasanja">3. Техничка опременост</td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa14" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa14" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa14" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa14" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa14" value="5" id="radio5"></td>
</tr>
<tr>
<td colspan="8" class="prasanja">4. Просторни услови</td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa15" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa15" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa15" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa15" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa15" value="5" id="radio5"></td>
</tr>
<tr>
<td colspan="8" class="prasanja">5. Почитување и заштита на правата на студентите</td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa16" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa16" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa16" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa16" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa16" value="5" id="radio5"></td>
</tr>
<tr>
<td colspan="8" class="prasanja">6. Вкупни услови за студирање</td>
<td colspan="5"><label for="radio1"></label>
<input type="radio" name="grupa17" value="1" id="radio1"></td>
<td colspan="5"><label for="radio2"></label>
<input type="radio" name="grupa17" value="2" id="radio2"></td>
<td colspan="5"><label for="radio3"></label>
<input type="radio" name="grupa17" value="3" id="radio3"></td>
<td colspan="5"><label for="radio4"></label>
<input type="radio" name="grupa17" value="4" id="radio4"></td>
<td colspan="5"><label for="radio5"></label>
<input type="radio" name="grupa17" value="5" id="radio5"></td>
</tr>
</tbody>
</table>
<div id="divSubmit">
<label for="datum">Се оценува на скала од 1 – 5  (1 најниска оценка)</label><br><br>
<input id="vnesiKopce" name="submit" type="submit" value="Внеси">
</div>
</div>
        </form>



    </body>
</html>
