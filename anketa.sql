-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 22, 2015 at 12:00 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `anketa`
--

-- --------------------------------------------------------

--
-- Table structure for table `anketa_predmeti_profesori`
--

CREATE TABLE IF NOT EXISTS `anketa_predmeti_profesori` (
  `id_anketa_profesori` int(11) NOT NULL AUTO_INCREMENT,
  `studiska_id_studiska_programa` int(11) NOT NULL,
  `predmet_id_predmet` int(11) NOT NULL,
  `profesor_id_profesor` int(11) NOT NULL,
  `pp1` int(11) NOT NULL,
  `pp2` int(11) NOT NULL,
  `pp3` int(11) NOT NULL,
  `pp4` int(11) NOT NULL,
  `pp5` int(11) NOT NULL,
  `pp6` int(11) NOT NULL,
  `pp7` int(11) NOT NULL,
  `pp8` int(11) NOT NULL,
  `pp9` int(11) NOT NULL,
  `pp10` int(11) NOT NULL,
  `pp11` int(11) NOT NULL,
  `pp12` int(11) NOT NULL,
  `pp13` int(11) NOT NULL,
  `pp14` int(11) NOT NULL,
  `pp15` int(11) NOT NULL,
  `pp16` int(11) NOT NULL,
  PRIMARY KEY (`id_anketa_profesori`),
  KEY `predmet_id_predmet` (`predmet_id_predmet`),
  KEY `studiska_id_studiska_programa` (`studiska_id_studiska_programa`),
  KEY `profesor_id_profesor` (`profesor_id_profesor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `anketa_studiska_programa`
--

CREATE TABLE IF NOT EXISTS `anketa_studiska_programa` (
  `vid_id_vid_na_studii` int(11) NOT NULL,
  `p1` int(11) NOT NULL,
  `p2` int(11) NOT NULL,
  `p3` int(11) NOT NULL,
  `p4` int(11) NOT NULL,
  `p5` int(11) NOT NULL,
  `p6` int(11) NOT NULL,
  `pp1` int(11) NOT NULL,
  `pp2` int(11) NOT NULL,
  `pp3` int(11) NOT NULL,
  `pp4` int(11) NOT NULL,
  `pp5` int(11) NOT NULL,
  `ppp1` int(11) NOT NULL,
  `ppp2` int(11) NOT NULL,
  `ppp3` int(11) NOT NULL,
  `ppp4` int(11) NOT NULL,
  `ppp5` int(11) NOT NULL,
  `ppp6` int(11) NOT NULL,
  `id_anketa_studiska` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_anketa_studiska`),
  KEY `vid_id_vid_na_studii` (`vid_id_vid_na_studii`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `predmeti`
--

CREATE TABLE IF NOT EXISTS `predmeti` (
  `Id_predmet` int(11) NOT NULL AUTO_INCREMENT,
  `Ime_predmet` char(50) CHARACTER SET utf8 DEFAULT NULL,
  `predmet_id_profesor` int(11) NOT NULL,
  PRIMARY KEY (`Id_predmet`),
  KEY `predmet_id_profesor` (`predmet_id_profesor`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `predmeti`
--

INSERT INTO `predmeti` (`Id_predmet`, `Ime_predmet`, `predmet_id_profesor`) VALUES
(1, 'Складишта на податоци', 2),
(2, 'CAD/CAM апликации', 1),
(3, 'Безбедност на компјутерскисистеми и мрежи', 21),
(4, 'Принципи на мултимедиски системи', 15),
(5, 'Менаџмент на софтверски проекти', 4),
(6, 'Софтверско инженерство', 4),
(7, 'Современи процесорски архитектури', 3),
(8, 'Податочно рударење', 7),
(9, 'Проектирање и развој на информациски системи', 18),
(10, 'Управување со информации и одлучување', 11);

-- --------------------------------------------------------

--
-- Table structure for table `profesori`
--

CREATE TABLE IF NOT EXISTS `profesori` (
  `Id_profesor` int(11) NOT NULL,
  `Ime` char(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`Id_profesor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profesori`
--

INSERT INTO `profesori` (`Id_profesor`, `Ime`) VALUES
(1, 'Александар Маркоски'),
(2, 'Виолета Маневска'),
(3, 'Пеце Митревски'),
(4, 'Илија Јолевски'),
(5, 'Игор Неделковски'),
(6, 'Драган Груевски'),
(7, 'Благој Ристевски'),
(8, 'Костадина Вељановска'),
(9, 'Моника Маркоска'),
(10, 'Соња Манчевска'),
(11, 'Снежана Савоска'),
(12, 'Андријана Боцевска'),
(13, 'Божидар Миленковски'),
(14, 'Елена Влаху-Ѓоргиевска'),
(15, 'Зоран Котевски'),
(16, 'Марина Блаженковиќ'),
(17, 'Мимоза Б.Јовановска'),
(18, 'Наташа Б.Табаковска'),
(19, 'Никола Рендевски'),
(20, 'Рамона Маркоска'),
(21, 'Томе Димовски'),
(22, 'лела Ивановска');

-- --------------------------------------------------------

--
-- Table structure for table `studenti`
--

CREATE TABLE IF NOT EXISTS `studenti` (
  `Br_Indeks` varchar(10) NOT NULL,
  `Ime` char(50) CHARACTER SET utf8 DEFAULT NULL,
  `Prezime` char(50) CHARACTER SET utf8 DEFAULT NULL,
  `username` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `semestar` int(11) DEFAULT NULL,
  `Studiska_programa` varchar(50) DEFAULT NULL,
  `vid_studii` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Br_Indeks`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `studenti`
--

INSERT INTO `studenti` (`Br_Indeks`, `Ime`, `Prezime`, `username`, `password`, `email`, `semestar`, `Studiska_programa`, `vid_studii`) VALUES
('2', 'Trajce', 'Trajkovski', 'trajce', 'trajce', 'trajce', 7, 'IMSA', 'A4'),
('24', 'Vesna', 'Ristevska', 'vesnaa', 'vesna', 'vesna', 7, 'KNI', 'A4'),
('302', 'Valentina', 'Alovska', 'valentina', 'valentina', 'valentina', 5, 'MIS', 'A4');

-- --------------------------------------------------------

--
-- Table structure for table `studiska_programa`
--

CREATE TABLE IF NOT EXISTS `studiska_programa` (
  `id_studiska_programa` int(11) NOT NULL AUTO_INCREMENT,
  `ime_studiska_programa` char(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id_studiska_programa`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `studiska_programa`
--

INSERT INTO `studiska_programa` (`id_studiska_programa`, `ime_studiska_programa`) VALUES
(1, 'КНИ'),
(2, 'ИКТ'),
(3, 'ИМСА'),
(4, 'МИС'),
(5, 'ИНКИ');

-- --------------------------------------------------------

--
-- Table structure for table `vid_na_studii`
--

CREATE TABLE IF NOT EXISTS `vid_na_studii` (
  `Id_vid_na_studii` int(11) NOT NULL AUTO_INCREMENT,
  `Vid_na_studii` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Godini` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id_vid_na_studii`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `vid_na_studii`
--

INSERT INTO `vid_na_studii` (`Id_vid_na_studii`, `Vid_na_studii`, `Godini`) VALUES
(1, 'Akademski cetirigodisni', '4'),
(2, 'Akademski trigodisni', '3'),
(3, 'Strucni cetirigodisni', '4'),
(4, 'Strucni trigodisni', '3');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `anketa_predmeti_profesori`
--
ALTER TABLE `anketa_predmeti_profesori`
  ADD CONSTRAINT `anketa_predmeti_profesori_ibfk_1` FOREIGN KEY (`predmet_id_predmet`) REFERENCES `predmeti` (`Id_predmet`),
  ADD CONSTRAINT `anketa_predmeti_profesori_ibfk_2` FOREIGN KEY (`studiska_id_studiska_programa`) REFERENCES `studiska_programa` (`id_studiska_programa`),
  ADD CONSTRAINT `anketa_predmeti_profesori_ibfk_3` FOREIGN KEY (`profesor_id_profesor`) REFERENCES `profesori` (`Id_profesor`);

--
-- Constraints for table `anketa_studiska_programa`
--
ALTER TABLE `anketa_studiska_programa`
  ADD CONSTRAINT `anketa_studiska_programa_ibfk_1` FOREIGN KEY (`vid_id_vid_na_studii`) REFERENCES `vid_na_studii` (`Id_vid_na_studii`);

--
-- Constraints for table `predmeti`
--
ALTER TABLE `predmeti`
  ADD CONSTRAINT `predmeti_ibfk_1` FOREIGN KEY (`predmet_id_profesor`) REFERENCES `profesori` (`Id_profesor`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
