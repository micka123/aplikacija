<?php
session_start();

if(isset($_SESSION['user'])!="")
{
 header("Location: home.php");
}

include_once 'Dbconnect.php';
mysqli_set_charset($con, 'utf-8');
if(isset($_POST['submit']))
{
 $ime = mysqli_real_escape_string($con, htmlentities ($_POST['ime']));
 $prezime = mysqli_real_escape_string($con, htmlentities($_POST['prezime']));
 $brIndeks = mysqli_real_escape_string($con, htmlentities($_POST['brIndeks']));
 $korisnicko_ime = mysqli_real_escape_string($con, htmlentities($_POST['korisnicko_ime']));
 $lozinka = mysqli_real_escape_string($con, htmlentities($_POST['lozinka']));
 $email = mysqli_real_escape_string($con, htmlentities($_POST['email']));
 $semestar = mysqli_real_escape_string($con, htmlentities($_POST['semestar']));
 $studiska_programa = mysqli_real_escape_string($con, htmlentities($_POST['studiska_programa']));
 $vid_studii = mysqli_real_escape_string($con, htmlentities($_POST['vid_studii']));
 
 if($con->query("INSERT INTO studenti(Br_Indeks, Ime, Prezime, username,password,email,semestar,studiska_programa,vid_studii) VALUES('$brIndeks','$ime','$prezime','$korisnicko_ime','$lozinka','$email','$semestar','$studiska_programa','$vid_studii')"))
 {
  ?>
        <script>alert('Успешна регистрација ');</script>
        <?php
        header("location: home.php");
 }
 else
 {
  ?>
        <script>alert('Грешка при регистрирањето');</script>
        <?php
 }
}
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;  charset=UTF-8">
        <title>Registracija</title>
        <link rel="stylesheet" type="text/css" href="myStyle.css">
    </head>
    <body>
        <form name="Registracija"  method="post">
            <div id="container">
                <div id="contentRegistracija">
                    <div id="header">
                        <p id="fax">Факултет за информатички и комуникациски технологии</p>
                        <p id="SoftverEvaluacija">Регистрација</p>
                    </div>
                    <div id="imePrezime">
                        <label>Име :</label>
                        <input class="inputIme" type="text" name="ime" placeholder="Внеси име"><br>
                    </div>
                    <div id="imePrezime">
                        <label>Презиме :</label>
                        <input class="inputImePrezime" type="text" name="prezime" placeholder="Внеси презиме"><br>
                    </div>
                    <div id="indeks">
                        <label>Број на индекс:</label>
                        <input class="inputIndeks" type="text" name="brIndeks" placeholder="Внеси број на индекс"><br>
                    </div>
                    <div id="indeks">
                        <label>Корисничко име:</label>
                        <input class="inputKorisnik" type="text" name="korisnicko_ime"
                               placeholder="Внеси корисничко име"><br>
                    </div>
                    <div id="indeks">
                        <label>Лозинка:</label>
                        <input class="inputLozinka2" type="password" name="lozinka" placeholder="Внеси лозинка"><br>
                    </div>
                    <div id="indeks">
                        <label>Email адреса:</label>
                        <input class="inputEmail" type="text" name="email" placeholder="Внеси email адреса"><br>
                    </div>
                    <div id="indeks">
                        <label>Семестар:</label>
                        <select id="inputSemestar" name="semestar"  >
                            <option selected disabled hidden value=''></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>    
                            <option value="5">5</option> 
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                        </select>

                    </div>
                    <div id="indeks">
                        <label>Студиска програма:</label>
                        <select id="inputPrograma" name="studiska_programa">
                            <option selected disabled hidden value=''></option>
                            <option value="KNI">КНИ</option>
                            <option value="IKT">ИКТ</option>
                            <option value="MIS">МИС</option>
                            <option value="IMSA">ИМСА</option>    
                            <option value="INKI">ИНКИ</option>    				
                        </select><br>
                    </div>
                    <div id="indeks">
                        <label>Вид на студии:</label>
                        <select id="inputStudii" name="vid_studii">
                            <option selected disabled hidden value=''></option>
                            <option value="A3">Академски тригодишни</option>
                            <option value="A4">Академски четиригодишни</option>
                            <option value="S3">Стручни тригодишни</option>
                            <option value="S4">Стручни четиригодишни</option>    

                        </select><br>
                    </div>
              
                    <input type="submit" name="submit" id="kopceRegistracija" value="Регистрирај Се" />
                    </form>
                    </body>
                    </html>
